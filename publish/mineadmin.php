<?php
declare(strict_types=1);

return [
    'version'=>'0.6.2',
    // 是否启用数据权限
    'data_scope_enabled' => true,
];
